<?php
namespace Cherrypulp\DataLayer;

/**
 * ServiceProvider.
 * Register the DataLayer helper class as a singleton into Laravel
 *
 * @author Anthony Pauwels <anthony@cherrypulp.com>
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->app->singleton('datalayer', function ($app) {
            return new DataLayer($app['session'], env('GOOGLE_ID'));
        });
    }
}
