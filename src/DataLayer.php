<?php
namespace Cherrypulp\DataLayer;

use Illuminate\Session\SessionManager;

/**
 * Helper class for handling DataLayer object with Google Tag Manager
 *
 * @author Anthony Pauwels <anthony@cherrypulp.com>
 * @author Stéphan Zych <stephan@cherrypulp.com>
 */
class DataLayer
{
    /** @var array */
    protected $data = [];
    /** @var SessionManager */
    protected $session;
    /** @var string */
    protected $google_id;

    /**
     * DataLayer constructor.
     *
     * @param SessionManager $session
     * @param $google_id
     */
    public function __construct(SessionManager $session, $google_id)
    {
        $this->session = $session;
        $this->google_id = $google_id;

        $this->load();
    }

    /**
     * Load data from the session
     */
    public function load()
    {
        $this->data = array_merge($this->session->get('datalayer', []), []);
    }

    /**
     * Clear all data from the array
     */
    public function clear()
    {
        $this->session->put('datalayer', []);
    }

    /**
     * Save the data into the session
     */
    public function save()
    {
        $this->session->put('datalayer', $this->data);
    }

    /**
     * Get the data array
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Push a value with name into the data array
     *
     * @example
     * DataLayer::push('name', 'value');
     * @example
     * DataLayer::push(['name' => 'value']);
     *
     * @param mixed $name
     * @param mixed $value
     * @param array $options
     */
    public function push($name, $value = null, array $options = ['echo' => false, 'session' => false])
    {
        if ( is_array($name) ) {
            $options = $value;
            $this->pushArray($name, $options);
        } elseif ( $options['echo'] ) {
            $this->scriptPush([$name => $value]);
        } else {
            $this->data[$name] = $value;

            if ($options['session']) {
                $this->save();
            }
        }
    }

    /**
     * Push a full array into the data
     *
     * @param array $data
     * @param array $options
     */
    public function pushArray($data, array $options = ['echo' => false, 'session' => false])
    {
        if ( $options['echo'] ) {
            $this->scriptPush($data);
        } else {
            $this->data = array_merge($this->data, $data);

            if ( $options['session'] ) {
                $this->save();
            }
        }
    }

    /**
     * Print the datalayer object into the page; options can be used to control the init and the script
     *
     * @param array $options
     */
    public function publish($options = ['init' => true, 'script' => true])
    {
        if ( isset($options['init']) && $options['init'] === true ) {
            self::init();
        }

        if ( !empty($this->data) ) {
            $this->scriptPush($this->data);
            $this->clear();
        }

        if ( isset($options['script']) && $options['script'] === true ) {
            $this->script($this->google_id);
        }
    }

    /**
     * Clear all data from the array at the end of the script
     */
    public function __destruct()
    {
        $this->data = [];
    }

    /**
     * Init the JS DataLayer object
     */
    public function init()
    {
        ?><script>window.dataLayer = window.dataLayer || [];</script><?php
    }

    /**
     * Print the google tag manager init script with given google id
     *
     * @param string|null $google_id
     */
    public function script($google_id = null)
    {
        if ( $google_id === null ) {
            $google_id = $this->google_id;
        }
        ?><!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?= $google_id ?>');</script>
        <!-- End Google Tag Manager --><?php
    }

    /**
     * Push the data into the JS DataLayer object
     *
     * @param array $data
     */
    public function scriptPush(array $data)
    {
        ?><script>
            window.dataLayer.push(<?= json_encode($data); ?>);
        </script><?php
    }

    /**
     * Print the google tag manager no-script tag with given google id
     *
     * @param string|null $google_id
     * @return void
     */
    public function noScript($google_id = null)
    {
        if ( $google_id === null ) {
            $google_id = $this->google_id;
        }
        ?><!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $google_id; ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) --><?php
    }

    /**
     * Dump the data; debug purpose
     */
    public function dd()
    {
        dd($this->data);
    }
}
