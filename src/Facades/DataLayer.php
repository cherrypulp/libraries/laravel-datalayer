<?php
namespace Cherrypulp\DataLayer\Facades;

/**
 * Facade.
 * Provide quick access methods to the DataLayer helper class
 *
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer load()
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer clear()
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer save()
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer getData()
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer push($name, $value = null, $options = ['echo' => false, 'session' => false])
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer pushArray($data, $options = ['echo' => false, 'session' => false])
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer publish($options = ['init' => true, 'script' => true])
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer init()
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer script($google_id = null)
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer scriptPush(array $data)
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer noScript($google_id = null)
 * @method static \Cherrypulp\DataLayer\Facades\DataLayer dd()
 *
 * @see \Cherrypulp\DataLayer\DataLayer
 * @author Anthony Pauwels <anthony@cherrypulp.com>
 */
class DataLayer extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'datalayer';
    }
}
