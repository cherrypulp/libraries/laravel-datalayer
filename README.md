# Laravel DataLayer

[![Build Status](https://travis-ci.org/cherrypulp/laravel-datalayer.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-datalayer)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/cherrypulp/laravel-datalayer/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/laravel-datalayer/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/cherrypulp/laravel-datalayer/badge.svg?branch=master)](https://coveralls.io/github/cherrypulp/laravel-datalayer?branch=master)

[![Packagist](https://img.shields.io/packagist/v/cherrypulp/laravel-datalayer.svg)](https://packagist.org/packages/cherrypulp/laravel-datalayer)
[![Packagist](https://poser.pugx.org/cherrypulp/laravel-datalayer/d/total.svg)](https://packagist.org/packages/cherrypulp/laravel-datalayer)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-datalayer.svg)](https://packagist.org/packages/cherrypulp/laravel-datalayer)

Package description: Laravel package that generating Google's DataLayer script

## Installation

Install via composer
```bash
composer require cherrypulp/laravel-datalayer
```

### Register your Google ID

Add this `` GOOGLE_ID=YOUR_GOOGLE_ID`` at the end of your .env file

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\DataLayer\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
'DataLayer' => Cherrypulp\DataLayer\Facades\DataLayer::class,
```

## Usage

### In your Controllers

#### Push one value in the DataLayer

```php
DataLayer::push('foo', 'bar');
```

#### Push an array of data in the DataLayer

```php
DataLayer::pushArray([
    'user_name' => 'John Doe',
    'age' => '42',
    'country' => 'Belgium',
]);
```

Don't hesitate to check the prototype of the method to view all possibles options 

### In your View

#### Publish the DataLayer in the view

Just call this method in your app layout before the closing HEAD tag.

```php
DataLayer::publish();
```

It will print this entire HTML code in your layout

```html
<script>
    dataLayer = dataLayer || [];
</script>

<script>
    dataLayer.push({foo:'bar',user_name:'John Doe',age:42,country:'Belgium'});
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','YOUR_GOOGLE_ID');</script>
<!-- End Google Tag Manager -->
```

Don't forget to call `DataLayer::noScript()` right after your BODY tag

```php
DataLayer::noScript();
```

It will print the following :

```html
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=YOUR_GOOGLE_ID"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
```

You can use an optional array to choose if you do not want to initialise the global JS object or/and the Google's GTM script

```php
DataLayer::publish(['init' => false, 'script' => false]);
```

It will just print this

```html
<script>
    dataLayer.push({foo:'bar',user_name:'John Doe',age:42,country:'Belgium'});
</script>
```

The DataLayer is reset after each call to the DataLayer::publish() method.

### Others methods

#### Load the data from session

```php
DataLayer::load();
```

#### Save the data in the session

```php
DataLayer::save();
```

#### Clear the data in the session

```php
DataLayer::clear();
```

#### Get the array data

```php
DataLayer::getData();
```

#### Print the global JS object in the view

```php
DataLayer::init();
```

It will print this in the HTML

```html
<script>
    window.dataLayer = window.dataLayer || [];
</script>
```

#### Print the Google's GTM script in the view

The $google_id parameter is optional. If omitted, it will use the Google ID set in your .env file

```php
DataLayer::script([$google_id = null]);
```

```html
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','YOUR_GOOGLE_ID');</script>
<!-- End Google Tag Manager -->
```

Also, don't forget to add the no-script tag with

```php
DataLayer::noScript([$google_id = null]);
```

```html
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=YOUR_GOOGLE_ID"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
```


#### Show the content of the DataLayer (debug purpose)

```php
DataLayer::dd();
```

## Security

If you discover any security related issues, please email anthony@cherrypulp.com
instead of using the issue tracker.

## Credits

- [Anthony Pauwels](https://cherrypulp.com/)
- [Stéphan Zych](https://cherrypulp.com/)
- [All contributors](https://github.com/cherrypulp/laravel-datalayer/graphs/contributors)

## See also

- [Dev Guide](https://developers.google.com/tag-manager/devguide)
- [Quick Start](https://developers.google.com/tag-manager/quickstart)
