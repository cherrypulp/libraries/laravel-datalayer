<?php

namespace Cherrypulp\Datalayer\Tests;

use Cherrypulp\Datalayer\Facades\DataLayer;
use Cherrypulp\Datalayer\ServiceProvider;
use Orchestra\Testbench\TestCase;

class DatalayerTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'laravel-datalayer' => DataLayer::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
